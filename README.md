# IaC with Terraform CI/CD

## Introduction

This repository is exploring using GitLab to trigger Terraform jobs. This solution is functional, but should have an integration with a "backend" where the Terraform state may be stored.


## Getting Started

- `.gitlab-ci.yml` - Runs a Terraform `fmt`, `validate`, `build`, `deploy`, `destroy`
- `main.tf` - Terraform file that loops across a local variable, `["indy", "henry", "marian", "katanga"]`


## Resources
- [Terraform by HashiCorp](https://www.terraform.io/)
- [GitLab - Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- [GitLab - .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab - .gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)


## Author
@RZFeeser - Author & Instructor - Feel free to reach out to if you're looking for a instructor led training solution.
